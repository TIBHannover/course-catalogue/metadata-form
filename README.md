# Metadata GitLab Form

Simple user interface to collect standard metadata and create a yml file.

* simple HTML/JS file, that form entries into a yml format

[Form](https://tibhannover.gitlab.io/course-catalogue/metadata-form/metadata-generator.html)
